# Portfolio project IDATA1003 - 2023
This file uses Mark Down syntax. For more information see [here](https://www.markdownguide.org/basic-syntax/).

STUDENT NAME = "Jonas Grønskag Johansen"  
STUDENT ID = "528476"

## Project description

[//]: # (TODO: Write a short description of your project/product here.)
This is a project done in the course IDATT1003. The objective of this prokect
is to develop a traindispatch system for one (1) trainstation. This includes 
showing multiple different train departures from this station with information
about departure time, destination, line, train number, track and possible delays.

## Project structure

[//]: # (TODO: Describe the structure of your project here. How have you used packages in your structure. Where are all sourcefiles stored. Where are all JUnit-test classes stored. etc.)
Within the src file on the there are two main folders, one containing the main code,
and another containing tests for the classes created. For example, there is a java class
file called TrainDeparture.java, and the corresponding test file is called
TrainDepartureTest.java. 


## How to run the project

[//]: # (TODO: Describe how to run your project here. What is the main class? What is the main method?
What is the input and output of the program? What is the expected behaviour of the program?)
Within the main folder there are four different files. The main file which the program
is being run through is called TrainDispatchApp.java. If you run this file you should be
greeted with a menu giving you 8 different options for what you want to do. When the user
chooses an option other than 8, they should return back to the menu after the task has
been completed. Pressing 8 closes the application.

## How to run the tests

[//]: # (TODO: Describe how to run the tests here.)
The tests are very simple as the user simply needs to run whichever test they want
and should receive instructions and explenations on what is being printed out in
the terminal.

