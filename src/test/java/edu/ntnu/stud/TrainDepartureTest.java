package edu.ntnu.stud;

import java.time.LocalTime;

public class TrainDepartureTest {
  /* The creation of the instances are made using chatGPT simply
     to make the test file quicker.*/
  public static void main(String[] args) {
    // Positive Test - Instance 1
    LocalTime departureTime1 = LocalTime.parse("12:30");
    String line1 = "F1";
    String trainNumber1 = "123";
    String destination1 = "Oslo";
    int track1 = 1;
    LocalTime delay1 = LocalTime.parse("00:15");

    TrainDeparture departure1 = new TrainDeparture(departureTime1, line1,
        trainNumber1, destination1, track1, delay1);

    // Positive Test - Instance 2
    LocalTime departureTime2 = LocalTime.parse("14:45");
    String line2 = "A3";
    String trainNumber2 = "456";
    String destination2 = "Tromsø";
    int track2 = 2;
    LocalTime delay2 = LocalTime.parse("00:20");

    TrainDeparture departure2 = new TrainDeparture(departureTime2, line2,
        trainNumber2, destination2, track2, delay2);

    // Positive Test - Instance 3
    LocalTime departureTime3 = LocalTime.parse("10:00");
    String line3 = "T1";
    String trainNumber3 = "789";
    String destination3 = "Trondheim";
    int track3 = 3;
    LocalTime delay3 = LocalTime.parse("00:10");

    TrainDeparture departure3 = new TrainDeparture(departureTime3, line3,
        trainNumber3, destination3, track3, delay3);

    // Positive Test - Instance 4
    LocalTime departureTime4 = LocalTime.parse("16:20");
    String line4 = "S7";
    String trainNumber4 = "101";
    String destination4 = "Stavanger";
    int track4 = -1; // No assigned track
    LocalTime delay4 = LocalTime.parse("00:00"); // No delay

    TrainDeparture departure4 = new TrainDeparture(departureTime4, line4,
        trainNumber4, destination4, track4, delay4);

    // Test getters and setters for all instances
    System.out.println("Instance 1: " + departure1);
    System.out.println("Instance 2: " + departure2);
    System.out.println("Instance 3: " + departure3);
    System.out.println("Instance 4: " + departure4);

    // Negative Test
    // Trying to create a departure with an invalid time format
    try {
      LocalTime invalidDepartureTime = LocalTime.parse("25:00"); // Invalid time
      TrainDeparture invalidDeparture = new TrainDeparture(invalidDepartureTime, line1, trainNumber2,
          destination3, track4, delay1);
    } catch (Exception e) {
      System.out.println("Error creating departure with invalid time: " + e.getMessage());
    }
  }
}
