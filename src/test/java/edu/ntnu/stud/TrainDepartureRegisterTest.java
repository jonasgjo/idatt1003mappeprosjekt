package edu.ntnu.stud;
import java.time.LocalTime;
import java.util.ArrayList;

public class TrainDepartureRegisterTest {

  /* The creation of the instances are made using chatGPT simply
     to make the test file quicker.*/
  public static void main(String[] args) {
    // Create a TrainDepartureRegister
    TrainDepartureRegister register = new TrainDepartureRegister();

    // Add some TrainDepartures
    TrainDeparture departure1 = new TrainDeparture(LocalTime.of(9, 0), "F1", "123", "Trondheim", 1, LocalTime.of(0, 15));
    TrainDeparture departure2 = new TrainDeparture(LocalTime.of(8, 30), "T2", "456", "Oslo", 2, LocalTime.of(0, 30));
    TrainDeparture departure3 = new TrainDeparture(LocalTime.of(10, 15), "G3", "789", "Stavanger", 3, LocalTime.of(0, 0));

    // Positive test case: Add departures to the register
    register.addTrainDeparture(departure1);
    register.addTrainDeparture(departure2);
    register.addTrainDeparture(departure3);

    // Positive test case: Print the register
    System.out.println("Train Departure Register:");
    System.out.println(register);

    // Positive test case: Search by train number
    System.out.println("\nPositive Test Case: Search by train number:");
    TrainDeparture foundDeparture = register.searchTrainNumber("456");
    if (foundDeparture != null) {
      System.out.println("Found departure: " + foundDeparture);
    }

    // Negative test case: Search by non-existent train number
    System.out.println("\nNegative Test Case: Search by non-existent train number:");
    foundDeparture = register.searchTrainNumber("999");
    if (foundDeparture == null) {
      System.out.println("Train not found. This is expected.");
    }

    // Positive test case: Search by destination
    System.out.println("\nPositive Test Case: Search by destination:");
    System.out.println("Found departures:");
    register.searchDestination("Destination1");


    // Negative test case: Search by non-existent destination
    System.out.println("\nNegative Test Case: Search by non-existent destination:");
    register.searchDestination("Nonexistent");

    System.out.println("No departures found. This is expected.");

    // Positive test case: Remove departures before a certain time
    System.out.println("\nPositive Test Case: Remove departures before 09:01:");
    register.setCurrentTime(LocalTime.parse("09:01"));
    register.removeTrainDepartures();
    System.out.println("Updated Train Departure Register:");
    System.out.println(register);

    // Positive test case: Sort departures
    System.out.println("\nPositive Test Case: Sort departures:");
    register.sortTrainDeparture();
    System.out.println(register);
  }
}

