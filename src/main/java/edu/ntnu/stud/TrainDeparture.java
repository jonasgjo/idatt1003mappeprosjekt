package edu.ntnu.stud;

import java.time.LocalTime;

/**
 * Entitiy class for train departures.
 * Its job is only creating a train departure and
 * formats the output.
 */
public class TrainDeparture {
  private final LocalTime departureTime;
  private final String line;
  private final String trainNumber;
  private final String destination;
  private int track;
  private LocalTime delay;

  /**
   * Constructor for trainDeparture.
   *
   * @param departureTime the time the train departs, format hh:mm.
   * @param line string describing what stretch the train is going.
   * @param trainNumber string containing a unique number, number is unique within
   *                    a 24 hour window.
   * @param destination the destination for the train.
   * @param track integer telling us which track the train is on.
   *              If it has no assigned track, set it to -1.
   * @param delay how much delayed the train is given as hh:mm. If no delay 00:00.
   */
  public TrainDeparture(LocalTime departureTime, String line, String trainNumber,
                        String destination, int track, LocalTime delay) {
    this.departureTime = departureTime;
    this.line = line;
    this.trainNumber = trainNumber;
    this.destination = destination;
    this.track = track;
    this.delay = delay;
  }

  public LocalTime getDepartureTime() {
    return departureTime;
  }

  public String getTrainNumber() {
    return trainNumber;
  }

  public String getDestination() {
    return destination;
  }

  public void setTrack(int newTrack) {
    this.track = newTrack;
  }

  public LocalTime getDelay() {
    return delay;
  }

  public void setDelay(LocalTime newDelay) {
    this.delay = newDelay;
  }

  @Override
  public String toString() {
    String tempTrack = String.valueOf(track);
    String tempDelay = String.valueOf(delay);

    if (track == -1) {
      tempTrack = "";
    }
    if (delay == LocalTime.parse("00:00")) {
      tempDelay = "";
    }

    return String.format("%14s|%5s|%13s|%12s|%7s|%6s",
        departureTime, line, trainNumber, destination, tempDelay, tempTrack);
  }
}
