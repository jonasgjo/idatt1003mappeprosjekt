package edu.ntnu.stud;


import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;


/**
 * TrainDepartureRegister is a register and keeps track
 * of all the train departures created by the TrainDeparture
 * class. It keeps track of all the departures by using
 * an ArrayList.
 * This class can add new departures, remove departures
 * by updating a timer, search for a departure by train
 * number, search for multiple departures by destination,
 * and sort departures by time.
 */
public class TrainDepartureRegister {
  private final ArrayList<TrainDeparture> register;
  private LocalTime currentTime;

  TrainDepartureRegister() {
    this.register = new ArrayList<>();
    this.currentTime = LocalTime.parse("00:00");
  }

  /**
   * Method for adding train departure to register. Checks if there is a
   * departure there that contains the same train number.
   *
   * @param newDeparture input from the menu creating a new instance of the
   *                     TrainDeparture class.
   */
  public void addTrainDeparture(TrainDeparture newDeparture) {
    for (TrainDeparture element : register) {
      if (element.getTrainNumber().matches(newDeparture.getTrainNumber())) {

        System.out.println(
            "-------------------------------------------------------\n"
                + "There already exist a departure with that train number.\n"
                + "Please try again!\n"
                + "-------------------------------------------------------"
        );
        return;
      }
    }
    register.add(newDeparture);
  }

  /**
   * Method for searching for a single train departure.
   *
   * @param trainNumber unique identifier used for searching
   *                    for the train departure with matching
   *                    train number.
   *
   * @return returns either the element of type TrainDeparture
   *      , or null if it can not find the departure with matching
   *      train number.
   */
  public TrainDeparture searchTrainNumber(String trainNumber) {
    for (TrainDeparture element : register) {
      if (element.getTrainNumber().matches(trainNumber)) {
        return element;
      }
    }
    System.out.println(
        "-------------------------------------------------------\n"
            + "Sorry, we couldn't find a departure with that train number.\n"
            + "Please try again.\n"
            + "-------------------------------------------------------"
    );

    return null;
  }

  /**
   * Method for searching for train departures going to a given
   * destination.
   *
   * @param destination of type String used for searching after
   *                    the departures.
   */
  public void searchDestination(String destination) {
    System.out.println("Departure time| Line| Train number| Destination| Delay| Track\n");
    for (TrainDeparture element : register) {
      if (element.getDestination().matches(destination)) {
        System.out.println(element);
      }
    }
  }

  /**
   * Removes train departures by looping through each departure
   * in the register and checking if the departureTime plus delay
   * is before or after the input parameter.
   */
  public void removeTrainDepartures() {
    Iterator<TrainDeparture> iterator = register.iterator();
    while (iterator.hasNext()) {
      TrainDeparture departure = iterator.next();
      LocalTime departureTime = LocalTime.of(departure.getDepartureTime().getHour(),
          departure.getDepartureTime().getMinute());
      LocalTime departureTimeWithDelay = departureTime.plusHours(departure.getDelay().getHour())
          .plusMinutes(departure.getDelay().getMinute());

      if (departureTimeWithDelay.isBefore(currentTime)) {
        // Remove the departure if its scheduled time is before the updated clock time
        iterator.remove();
      }
    }
  }

  /**
   * Sorts the departures in register by looking at the
   * departure time + delay and then prints them out to
   * the terminal.
   */
  public void sortTrainDeparture() {
    register.sort(Comparator.comparing(td -> {
      LocalTime departureTime = LocalTime.of(td.getDepartureTime().getHour(),
          td.getDepartureTime().getMinute());
      return departureTime.plusHours(
          td.getDelay().getHour()).plusMinutes(td.getDelay().getMinute());
    }));
  }

  /**
   * Sets the new time for the train station.
   *
   * @param newCurrentTime of type LocalTime and is used for
   *                       setting a new time if it is after
   *                       the already established time.
   */
  public void setCurrentTime(LocalTime newCurrentTime) {
    if (newCurrentTime.isBefore(currentTime)) {
      System.out.println(
          "You can't set the clock backwards. Please try again."
      );
      return;
    }
    currentTime = newCurrentTime;
    System.out.print("New time has been set. ");
    System.out.println(currentTime);
  }

  @Override
  public String toString() {
    StringBuilder formattedString = new StringBuilder(
        "Departure time| Line| Train number| Destination| Delay| Track\n"
    );
    for (TrainDeparture element : register) {
      formattedString.append(element).append("\n");
    }
    return formattedString.toString();
  }
}
