package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.Scanner;

/**
 * Class for the menu.
 * This tackles the menu interactions the user will have
 */
public class Menu {

  private final TrainDepartureRegister register;

  public Menu(TrainDepartureRegister register) {
    this.register = register;
  }

  /**
   * Prints the menu options for the user.
   */
  private void init() {
    System.out.println("-------------------------------------------------------");
    System.out.println("Hello what do you wish to do?");
    System.out.println("1: View train departures");
    System.out.println("2: Add new train departure");
    System.out.println("3: Add or change track for a given train");
    System.out.println("4: Add delay on a given train departure");
    System.out.println("5: Search for train departure");
    System.out.println("6: Search for train departures to a given destination");
    System.out.println("7: Update the clock");
    System.out.println("8: Exit the application");
    System.out.println("-------------------------------------------------------");
  }

  /**
   * This method uses the init() method to show
   * the user what their options and looping through
   * the menu options.
   */
  public void start() {
    Scanner in = new Scanner(System.in);

    while (true) {
      init();
      String menuChoice = in.nextLine();

      switch (menuChoice) {
        case "1":
          register.removeTrainDepartures();
          register.sortTrainDeparture();
          System.out.println(register);
          break;
        case "2":
          addNewTrainDeparture(in);
          break;
        case "3":
          changeTrack(in);
          break;
        case "4":
          changeDelay(in);
          break;
        case "5":
          searchDepartureTrainNumber(in);
          break;
        case "6":
          searchDeparturesDestination(in);
          break;
        case "7":
          updateTime(in);
          break;
        case "8":
          in.close();
          goodbye();
          break;
        default:
          System.out.println(
              "-------------------------------------------------------\n"
              + "Not an acceptable input, try again\n"
              + "-------------------------------------------------------"
          );
          break;
      }
    }
  }

  /**
   * Asks the user for different inputs and then uses all of
   * these to create a new departure and add it to the register.
   *
   * @param in Brings in the scanner making it possible for
   *           the program to take in user input
   */
  private void addNewTrainDeparture(Scanner in) {
    String timeFormat = "^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$";
    System.out.println("Whats the departure time (hh:mm)");

    String departureTimeStr = in.nextLine();
    if (!departureTimeStr.matches(timeFormat)) {
      System.out.println(
          "-------------------------------------------------------\n"
          + "Not a valid input, please use the format hh:mm\n"
          + "-------------------------------------------------------"
      );
      return;
    }
    final LocalTime departureTime = LocalTime.parse(departureTimeStr);

    System.out.println("What line");
    final String line = in.nextLine();
    if (line.isBlank()) {
      System.out.println(
          "-------------------------------------------------------\n"
          + "You can not leave this empty\n"
          + "-------------------------------------------------------"
      );
      return;
    }

    System.out.println("Train number");
    final String trainNumber = in.nextLine();
    //  "\\d+" checks if the input are numbers
    if (trainNumber.isBlank() || !trainNumber.matches("\\d+")) {
      System.out.println(
          "-------------------------------------------------------\n"
          + "Invalid input for train number"
          + ", can not be empty or contain letters\n"
          + "-------------------------------------------------------"
      );
      return;
    }

    System.out.println("Destination");
    final String destination = in.nextLine();
    if (destination.isBlank()) {
      System.out.println(
          "-------------------------------------------------------\n"
          + "Destination can not be empty\n"
          + "-------------------------------------------------------"
      );
      return;
    }

    System.out.println("What track (Optional, can leave as blank)");
    //Checks if input is blank or not
    String optional = in.nextLine();
    int track = -1;

    if (!optional.isBlank()) {
      if (optional.matches("\\d+")) {
        track = Integer.parseInt(optional);
      } else {
        System.out.println(
            "-------------------------------------------------------\n"
                + "Track can not be a combination of letters\n"
                + "-------------------------------------------------------"
        );
        return;
      }
    }

    System.out.println("What is the delay (hh:mm) (Optional, can leave as blank)");
    optional = in.nextLine();
    LocalTime delay = LocalTime.parse("00:00");

    if (!optional.isBlank()) {
      if (optional.matches(timeFormat)) {
        delay = LocalTime.parse(optional);
      } else {
        System.out.println(
            "-------------------------------------------------------\n"
            + "Not a valid input, please use the format hh:mm\n"
            + "-------------------------------------------------------"
        );
        return;
      }
    }


    TrainDeparture newTrainDeparture = new TrainDeparture(departureTime, line, trainNumber,
        destination, track, delay);

    register.addTrainDeparture(newTrainDeparture);
  }

  /**
   * Asks the user for the train number and new track number
   * then calls the searchTrainNumber method with the user
   * given train number to find the correct departure. If
   * the departure with that train number exists then it
   * should proceed to set the track number by using the
   * method setTrack. If the searchTrainNumber returns
   * null, which means it could not find the departure,
   * the method should not do anything.
   *
   * @param in Brings in the scanner making it possible for
   *           the program to take in user input
   */
  private void changeTrack(Scanner in) {
    System.out.println("What is the train number?");
    String trainNumber = in.nextLine();
    System.out.println("What is the new track number?");
    int newTrackNumber = in.nextInt();
    in.nextLine();
    TrainDeparture departure = register.searchTrainNumber(trainNumber);

    /*Needs to check if there even is a departure with that train number.
    Will return null if it does not exist in the register*/
    if (departure != null) {
      departure.setTrack(newTrackNumber);
      System.out.println("All done!");
    }
  }

  /**
   * Changes the delay of a departure by asking for the
   * train number and then the new delay. Like changeTrack(),
   * it will check if there exists a departure with that
   * train number and then proceed to change the delay of
   * that departure if it exists, and do nothing if it
   * doesn't.
   *
   * @param in Brings in the scanner making it possible for
   *           the program to take in user input
   */
  private void changeDelay(Scanner in) {
    System.out.println("What is the train number?");
    String trainNumber = in.nextLine();
    System.out.println("What do you wish to change the delay to?");
    LocalTime newDelay = LocalTime.parse(in.nextLine());

    TrainDeparture departure = register.searchTrainNumber(trainNumber);

    /*Needs to check if there even is a departure with that train number.
    Will return null if it does not exist in the register*/
    if (departure != null) {
      departure.setDelay(newDelay);
      System.out.println("All done!");
    }
  }

  /**
   * Asks the user for the unique train number and uses
   * searchTrainNumber to check if it exists in the
   * register, and if it does then it prints out the
   * departure and does nothing if it doesn't exist.
   *
   * @param in Brings in the scanner making it possible for
   *           the program to take in user input
   */
  private void searchDepartureTrainNumber(Scanner in) {
    System.out.println("What is the train number?");
    String trainNumber = in.nextLine();

    TrainDeparture departure = register.searchTrainNumber(trainNumber);

    if (departure != null) {
      System.out.println("Departure time| Line| Train number| Destination| Delay| Track\n");
      System.out.println(departure);
    }
  }

  /**
   * Asks the user for a destination and then uses the method
   * searchDestination to find and print out all the
   * departures going to the destination.
   *
   * @param in Brings in the scanner making it possible for
   *           the program to take in user input
   */
  private void searchDeparturesDestination(Scanner in) {
    System.out.println("To what destination do you wish to check?");
    String destination = in.nextLine();
    register.searchDestination(destination);
  }

  /**
   * Asks the user for the new time to set the clock to
   * and first checks if the input matches the hh:mm
   * format. If it passes then it will run the
   * setCurrentTime method. If it fails the check
   * then it will print out an error message and
   * take the user back to the menu.
   *
   * @param in Brings in the scanner making it possible for
   *           the program to take in user input
   */
  private void updateTime(Scanner in) {
    System.out.println("What is the new time?");
    String newCurrentTimeStr = in.nextLine();
    if (!newCurrentTimeStr.matches("^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$")) {
      System.out.println(
          "-------------------------------------------------------\n"
          + "Please give a valid input (hh:mm)\n"
          + "-------------------------------------------------------"
      );
      return;
    }
    LocalTime newCurrentTime = LocalTime.parse(newCurrentTimeStr);
    register.setCurrentTime(newCurrentTime);
  }

  /**
   * Prints out the goodbye message when the
   * "close application" choice is chosen in
   * the menu, as well as closes the program.
   */
  private void goodbye() {
    System.out.println(
        "-------------------------------------------------------\n"
            + "Thanks for using our services, hope to see you soon!\n"
            + "-------------------------------------------------------"
    );
    System.exit(0);
  }
}
