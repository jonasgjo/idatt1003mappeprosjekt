package edu.ntnu.stud;


/**
 * This is the main class for the train dispatch application.
 */
public class TrainDispatchApp {
  /**
   * Main program and runs the menu from Menu.java
   */
  public static void main(String[] args) {
    TrainDepartureRegister register = new TrainDepartureRegister();
    Menu menu = new Menu(register);
    menu.start();
  }
}
